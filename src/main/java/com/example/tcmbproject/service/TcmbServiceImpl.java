package com.example.tcmbproject.service;

import com.example.tcmbproject.entities.Currency;

import java.util.List;

public interface TcmbServiceImpl {

 void   save (Currency currency) ;
 List<Currency> listAll();
}
