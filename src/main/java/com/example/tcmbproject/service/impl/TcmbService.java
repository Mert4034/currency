package com.example.tcmbproject.service.impl;

import com.example.tcmbproject.entities.Currency;
import com.example.tcmbproject.repository.CurrencyRepository;
import com.example.tcmbproject.service.TcmbServiceImpl;
import lombok.RequiredArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@RequiredArgsConstructor
@Service
@Transactional
public class TcmbService implements TcmbServiceImpl {
    private static final Logger logger = LoggerFactory.getLogger(TcmbService.class);

    private final CurrencyRepository currencyRepository;
    private int year = 2021;
    private int month;
    private int day;


    public List<Currency> todayXml() {

        List<Currency> clist = new ArrayList<>();

        for (month=6;month<9;month++){
            for (day=1;day<31;day++){
                if(day<10){
                    try {

                        String url = "https://www.tcmb.gov.tr/kurlar/"+year+"0"+month+"/"+"0"+day+"0"+""+month+""+year+".xml";
                        String stData = Jsoup.connect(url).timeout(60000).ignoreContentType(true).get().toString();
                        Document doc = Jsoup.parse(stData, "", Parser.xmlParser());
                        Elements elements = doc.getElementsByTag("Currency");
                        for (Element element : elements) {
                            String currencyName = element.getElementsByTag("CurrencyName").text();
                            String forexBuying = element.getElementsByTag("ForexBuying").text();
                            String forexSelling = element.getElementsByTag("ForexSelling").text();
                            String banknoteBuying = element.getElementsByTag("BanknoteBuying").text();
                            String banknoteSelling = element.getElementsByTag("BanknoteSelling").text();
                            String currencyDate = "0"+day+".0"+month+"."+year+"";
                            Currency currency = new Currency(currencyName, forexBuying, forexSelling, banknoteBuying, banknoteSelling, currencyDate);
                            save(currency);
                            clist.add(currency);
                        }

                    } catch (Exception e) {
                        System.err.println("todayXml Error : " + e);
                    }

                }
                else{
                    try {

                        String url = "https://www.tcmb.gov.tr/kurlar/"+year+"0"+month+"/"+day+"0"+""+month+""+year+".xml";
                        String stData = Jsoup.connect(url).timeout(60000).ignoreContentType(true).get().toString();
                        Document doc = Jsoup.parse(stData, "", Parser.xmlParser());
                        Elements elements = doc.getElementsByTag("Currency");
                        for (Element element : elements) {
                            String currencyName = element.getElementsByTag("CurrencyName").text();
                            String forexBuying = element.getElementsByTag("ForexBuying").text();
                            String forexSelling = element.getElementsByTag("ForexSelling").text();
                            String banknoteBuying = element.getElementsByTag("BanknoteBuying").text();
                            String banknoteSelling = element.getElementsByTag("BanknoteSelling").text();
                            String currencyDate = ""+day+".0"+month+"."+year+"";
                            Currency currency = new Currency(currencyName, forexBuying, forexSelling, banknoteBuying, banknoteSelling, currencyDate);
                            save(currency);
                            clist.add(currency);
                        }

                    } catch (Exception e) {
                        System.err.println("todayXml Error : " + e);
                    }

                }
            }
        }
        return clist;
    }

    @Override
    public void save(Currency currency) {

      currencyRepository.save(currency);
    }

    @Override
    public List<Currency> listAll() {
        List<Currency> currencies = currencyRepository.findAll();

        return currencies;
    }

}
