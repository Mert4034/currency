package com.example.tcmbproject.entities;

import lombok.*;

import javax.persistence.*;

@Data
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "currency", catalog = "currency")

public class Currency {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "currency_id", nullable = false)

    private Integer currencyId;
    private String CurrencyName;
    private String ForexBuying ;
    private String ForexSelling;
    private String BanknoteBuying;
    private String BanknoteSelling;
    private String CurrencyDate;

    public Currency(String currencyName, String forexBuying, String forexSelling, String banknoteBuying,
                    String banknoteSelling, String currencyDate) {
        super();
        CurrencyName = currencyName;
        ForexBuying = forexBuying;
        ForexSelling = forexSelling;
        BanknoteBuying = banknoteBuying;
        BanknoteSelling = banknoteSelling;
        CurrencyDate = currencyDate;
    }

}
