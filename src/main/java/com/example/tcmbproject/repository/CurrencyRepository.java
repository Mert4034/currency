package com.example.tcmbproject.repository;

import com.example.tcmbproject.entities.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository <Currency, Integer>{
}
