package com.example.tcmbproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TcmbProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(TcmbProjectApplication.class, args);
    }

}
